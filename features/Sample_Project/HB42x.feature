Feature: HB42 Security Test cases

  @Security @ClickJacking @1
  Scenario: To verify whether Internal User Login page opens in an iframe
    Given User open the html file in any browser
    Then check whether the site is vulnerale or not

  @Security @StrictHTTP @2
  Scenario: To verify security of Internal User Login of HB42 application
    Given User opens the browser and launch the HB42 Portal URl
    When User changes https to http
    Then User must be redirected to actual URL

  @Security @LockedOut @4
  Scenario: To verify that user gets locked out after making invalid attempts
    Given   User opens the browser and launch the HB42 Portal URl
    When try to login with incorrect username or password
    And try to login with incorrect username or password
    And try to login with incorrect username or password
    And try to login with incorrect username or password
    And try to login with incorrect username or password
    And try to login with incorrect username or password
    And try to login with incorrect username or password
    And try to login with incorrect username or password
    And try to login with incorrect username or password
    And try to login with incorrect username or password
    And try to login with incorrect username or password
    And try to login with incorrect username or password
    Then User enters valid credentials

  @Security @SessionTimeout @5
  Scenario: To verify session timeout in Internal User Login of HB42 application
    Given User opens the browser and launch the HB42 Portal URl
    When User logs in with valid credentials and does not use for specified time limit
    Then Application has automatically logged you out

  @Security @ErrorHandling @6
  Scenario: To verify the error message after making invalid attempt on Internal user login page.
    Given User opens the browser and launch the HB42 Portal URl
    When User enters incorrect login credentials
    Then User must see message Either Username or Password incorrect

  @Security @CookieStorage @7
  Scenario: To verify that CookieStorage of the application is correect
    Given User opens the browser and launch the HB42 Portal URl
    When User enters a different new url

  @Security @CookieStorage2 @4
  Scenario: To verify that a ppt is not able to access the file/page of the application before login
    Given User opens the browser and launch the HB42 Portal URl
    Then User enters valid credentials
    Then User logs out and go back

  @Security @SQLInjection @9
    Scenario: To verify that the application does not disclose information when SQL Queries are injected in The URL of the application
    Given User opens the browser and launch the HB42 Portal URl
    When User enters sql url
    And User must see error message

  @Security @Crosssite @10
    Scenario: To verify that the application is vulnerable for crosssite scripting or not
    Given User opens the browser and launch the HB42 Portal URl
    Then User enters valid credentials
    Then User clicks on link and tries to enter xss script

















