var data = require('../../TestResources/HB42Data');
var Objects = require(__dirname + '/../../repository/HB42page.js');
//var action = require(__dirname + '/../../features/step_definitions/ReusableTests.js');
var fs = require('fs');

var LoginPageHB42,DashboardPage;
function initializePageObjects(browser, callback) {
    var BPPage = browser.page.HB42page();
    LoginPageHB42 = BPPage.section.LoginPageHB42;
    DashboardPage= BPPage.section.DashboardPage;
    callback();
}
//1
module.exports = function() {

    this.Given(/^User opens the browser and launch the HB42 Portal URl$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
         console.log('Test Environment: ' + execEnv);
         if (execEnv.toUpperCase() == "QA") {
             URL = data.HB42URL1;
             initializePageObjects(browser, function () {
                 browser.maximizeWindow()
                     .deleteCookies()
                     .url(URL);
                 browser.timeoutsImplicitWait(30000);
                 LoginPageHB42.waitForElementVisible('@username', data.wait);
             });
         }
    });

    //3
    this.When(/^try to login with incorrect username or password$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QA" ) {
            LoginPageHB42.setValue('@username',data.username1)
                .setValue('@password',data.password1)
            .click('@LoginButton');
        }
    });

    this.Then(/^User enters valid credentials$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QA" ) {
            LoginPageHB42.setValue('@username',data.Username)
                .setValue('@password',data.Password)
            .click('@LoginButton');
        }
    });

    this.Then(/^User clicks on link and tries to enter xss script$/, function () {
        var URL;
            browser = this;
            var execEnv = data["TestingEnvironment"];
            if (execEnv.toUpperCase() == "QA" ) {
                initializePageObjects( browser,function () {
                    LoginPageHB42.waitForElementVisible('@clickbutton',10000)
                        .click('@clickbutton');
                    LoginPageHB42.waitForElementVisible('@clickpage',10000)
                        .click('@clickpage');
                    LoginPageHB42.waitForElementVisible('@editperson',10000)
                        .click('@editperson');
                    LoginPageHB42.waitForElementVisible('@editadd',10000).setValue('@editadd',data.xss)
                        .click('@editsub');
                    browser.pause(5000);
                     });
            }
    });

    this.When(/^User enters a different url$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QA") {
            URL = data.HB42URL2;
            initializePageObjects(browser, function () {
                browser.maximizeWindow()
                    .deleteCookies()
                    .url(URL);
                browser.timeoutsImplicitWait(30000);
                LoginPageHB42.waitForElementVisible('@username', data.wait);
            });
        }
    });

    this.When(/^User enters a different new url$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QA") {
            URL = data.HB42URL3;
            initializePageObjects(browser, function () {
                browser.maximizeWindow()
                    .deleteCookies()
                    .url(URL);
                browser.timeoutsImplicitWait(30000);
                LoginPageHB42.waitForElementVisible('@username', data.wait);
            });
        }
    });

    this.When(/^User enters incorrect login credentials$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QA" ) {
            LoginPageHB42
                .setValue('@username',data.incusername)
                .setValue('@password',data.incpassword)
                .click('@LoginButton');
        }
    });

    this.Then(/^User must see message Either Username or Password incorrect$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QA" ) {
            LoginPageHB42.waitForElementVisible('@incorrectPassword',data.wait);
        }
    });

   this.Given(/^User open the html file in any browser$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QA" ) {
            URL=data.HB42HTML;
            initializePageObjects(browser, function () {
                browser.maximizeWindow()
                    .deleteCookies()
                    .url(URL);

            });
        }
    });

    this.When(/^check whether the site is vulnerale or not$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QA" ) {
            var vulnerableText= "//p[contains(text(),'This website is vulnerable to Clickjacking')]";

               browser.pause(5000)
            browser.useXpath().waitForElementPresent(vulnerableText, data.wait);

        }
    });

    this.When(/^User changes https to http$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QA") {
            URL = data.HRAXeosURL3;
            initializePageObjects(browser, function () {
                browser.maximizeWindow()
                    .deleteCookies()
                    .url(URL);
                browser.timeoutsImplicitWait(30000);
                          });
        }
    });

    this.Then(/^User must be redirected to actual URL$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QA" ) {
            LoginPageHB42.waitForElementVisible('@username',data.wait)
                .waitForElementVisible('@password',data.wait);
        }
    });


//8
    this.When(/^User logs in with valid credentials and does not use for specified time limit$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QA" ) {
            LoginPageHB42.setValue('@username',data.Username)
                .setValue('@password',data.Password)
                .click('@LoginButton');
            browser.pause(1200000)
            .refresh();
        }
    });

    this.When(/^Application has automatically logged you out$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QA" ) {
            LoginPageHB42.waitForElementVisible('@username', data.longwait)
                .waitForElementVisible('@password', data.longwait);
        }
    });

    //9
    this.Then(/^User logs out and go back$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QA" ) {
            initializePageObjects( browser,function () {
              // browser.pause(3000)
                LoginPageHB42.click('@LogoutButton');
               // browser.navigate.back();

                browser.pause(5000)
                browser.back();
                            });
        }
    });



    this.When(/^User enters sql url$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QA") {
            URL = data.SQLUrl;
            initializePageObjects(browser, function () {
                browser.maximizeWindow()
                    .deleteCookies()
                    .url(URL);
                browser.timeoutsImplicitWait(30000);
            });
        }
    });

    this.Then(/^User must see error message$/, function () {
                browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QA" ) {
            DashboardPage.waitForElementVisible('@ErrorEncountered', data.wait)
                .assert.containsText('@ErrorEncountered','The resource you are looking for might have been removed, had its name changed, or is temporarily unavailable');

        }
    });
}