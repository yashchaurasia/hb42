module.exports = {
    sections: {
        LoginPageHB42 : {
            selector: 'body',
            elements: {
                username: {locateStrategy:'xpath',selector: "//input[@name='username']"},
                password: {locateStrategy:'xpath',selector: "//input[@name='password']"},
                LoginButton: {locateStrategy:'xpath',selector: "//input[@name='login']"},
                LogoutButton: {locateStrategy:'xpath',selector: "//a[contains(.,'Logout')]"},
                incorrectPassword: {locateStrategy:'xpath',selector: "//*[@class='txtError']"},
                Vulnerable: {locateStrategy:'xpath',selector: "//p[contains(text(),'This website is vulnerable to Clickjacking')]"},
                ServerError: {locateStrategy:'xpath',selector: "//h1[contains(text(),'Server Error')]"},
                clickbutton : {selector: "#HW img"},
                clickpage: {locateStrategy:'xpath',selector:"//span[@id='ctl00_MainBody_ShowHomeBodyMenu']/table/tbody/tr/td/table/tbody/tr/td/ul/li[1]"},
                editperson : {locateStrategy:'xpath',selector: "//*[@id='ctl00_MainBody_EditImageButton']"},
                editadd: {locateStrategy:'xpath',selector: "//*[@id='ctl00_MainBody_PersonalDataControl_EmailAddress']"},
                editsub: {locateStrategy:'xpath',selector: "//*[@id='ctl00_MainBody_SubmitImageButton']"}
            },},
        DashboardPage: {
            selector: 'body',
            elements: {
                ErrorEncountered: {locateStrategy:'xpath',selector: "//*/[contains(text(),'The resource you are looking for might have been removed, had its name changed, or is temporarily unavailable')]"}
            }

        }
            }}